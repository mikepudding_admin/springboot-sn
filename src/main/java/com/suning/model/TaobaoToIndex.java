package com.suning.model;

public class TaobaoToIndex {
    private Integer id;
    private Integer car_id;
    private Long tb_id;
    private String tb_url;
    private String tb_msg;

    public TaobaoToIndex() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCar_id() {
        return car_id;
    }

    public void setCar_id(Integer car_id) {
        this.car_id = car_id;
    }

    public Long getTb_id() {
        return tb_id;
    }

    public void setTb_id(Long tb_id) {
        this.tb_id = tb_id;
    }

    public String getTb_url() {
        return tb_url;
    }

    public void setTb_url(String tb_url) {
        this.tb_url = tb_url;
    }

    public String getTb_msg() {
        return tb_msg;
    }

    public void setTb_msg(String tb_msg) {
        this.tb_msg = tb_msg;
    }
}
