package com.suning.model;

public class YtCarsToIndexCopy1 {
    private int id;
    private String car_id;
    private String sun_id;
    private int sun_status;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCar_id() {
        return car_id;
    }

    public void setCar_id(String car_id) {
        this.car_id = car_id;
    }

    public String getSun_id() {
        return sun_id;
    }

    public void setSun_id(String sun_id) {
        this.sun_id = sun_id;
    }

    public int getSun_status() {
        return sun_status;
    }

    public void setSun_status(int sun_status) {
        this.sun_status = sun_status;
    }
}
