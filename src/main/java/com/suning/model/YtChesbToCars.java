package com.suning.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class YtChesbToCars {
    @Id
    @GeneratedValue
    private Integer id;
    private String car_id;
    private String che300_id;
    private String che300_url;
    private int is_show;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCar_id() {
        return car_id;
    }

    public void setCar_id(String car_id) {
        this.car_id = car_id;
    }

    public String getChe300_id() {
        return che300_id;
    }

    public void setChe300_id(String che300_id) {
        this.che300_id = che300_id;
    }

    public String getChe300_url() {
        return che300_url;
    }

    public void setChe300_url(String che300_url) {
        this.che300_url = che300_url;
    }

    public int getIs_show() {
        return is_show;
    }

    public void setIs_show(int is_show) {
        this.is_show = is_show;
    }
}
