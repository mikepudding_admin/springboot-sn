package com.suning.model;

public class Cars {
    private int id;
    private String car_id;
    private String sun_id;
    private String sun_url;
    private String sun_msg;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCar_id() {
        return car_id;
    }

    public void setCar_id(String car_id) {
        this.car_id = car_id;
    }

    public String getSun_id() {
        return sun_id;
    }

    public void setSun_id(String sun_id) {
        this.sun_id = sun_id;
    }

    public String getSun_url() {
        return sun_url;
    }

    public void setSun_url(String sun_url) {
        this.sun_url = sun_url;
    }

    public String getSun_msg() {
        return sun_msg;
    }

    public void setSun_msg(String sun_msg) {
        this.sun_msg = sun_msg;
    }
}
