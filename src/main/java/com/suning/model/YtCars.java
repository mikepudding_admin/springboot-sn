package com.suning.model;

public class YtCars {
    private int id;
    private String car_model;
    private String car_brand;
    private String car_city;

    public String getCar_city() {
        return car_city;
    }

    public void setCar_city(String car_city) {
        this.car_city = car_city;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCar_model() {
        return car_model;
    }

    public void setCar_model(String car_model) {
        this.car_model = car_model;
    }

    public String getCar_brand() {
        return car_brand;
    }

    public void setCar_brand(String car_brand) {
        this.car_brand = car_brand;
    }
}
