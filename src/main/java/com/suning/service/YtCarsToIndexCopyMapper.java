package com.suning.service;

import com.suning.model.TaobaoToIndex;
import com.suning.model.YtCarsToIndexCopy1;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

@Mapper
public interface YtCarsToIndexCopyMapper {
    @Insert("INSERT INTO yt_cars_to_index_copy1(car_id,sun_id,sun_status) values (#{car_id},#{sun_id},#{sun_status})")
    public void addIndex(YtCarsToIndexCopy1 ytCarsToIndexCopy1);

    @Select("SELECT * FROM yt_cars_to_index_copy1 WHERE(car_id = #{car_id})")
    public YtCarsToIndexCopy1 searchByCarId(String car_id);

    @Select("SELECT * FROM yt_cars_to_index WHERE (tb_id is not null AND tb_status is null)")
    public List<TaobaoToIndex> searchTb();

    @Update("UPDATE yt_cars_to_index SET tb_status = 2,tb_msg = #{msg} WHERE id = #{id}")
    public void updateTbStatus(String msg,int id);
}
