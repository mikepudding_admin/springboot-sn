package com.suning.service;


import com.suning.model.Cars;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

@Mapper
public interface CarsMapper {
    @Select("select id,car_id,sun_id from yt_cars_to_index where sun_id is null")
    public List<Cars> selectAllCarsIndex();

    @Select("select * from yt_cars_to_index where sun_id is not null")
    public List<Cars> selectCarsNotNull();

    @Update("update yt_cars_to_index set sun_id = #{sun_id},sun_msg = #{msg} where id = #{id}")
    public void updateCarsById(String sun_id,String msg,int id);
}
