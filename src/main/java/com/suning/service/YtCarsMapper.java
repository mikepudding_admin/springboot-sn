package com.suning.service;

import com.suning.model.YtCars;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface YtCarsMapper {
    @Select("select * from yt_cars where id = #{id}")
    public YtCars selectYtCarsById(int id);
}
