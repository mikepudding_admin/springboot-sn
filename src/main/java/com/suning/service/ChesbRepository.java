package com.suning.service;

import com.suning.model.YtChesbToCars;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ChesbRepository extends JpaRepository<YtChesbToCars,Integer> {
}
