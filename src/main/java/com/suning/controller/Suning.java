package com.suning.controller;

import com.alibaba.fastjson.JSONObject;
import com.suning.api.DefaultSuningClient;
import com.suning.api.entity.custom.*;
import com.suning.api.entity.integrate.itemContentsModifyRequest;
import com.suning.api.entity.integrate.itemContentsModifyResponse;
import com.suning.api.entity.item.*;
import com.suning.api.exception.SuningApiException;
import com.suning.model.Cars;
import com.suning.model.YtCars;
import com.suning.service.CarsMapper;
import com.suning.service.YtCarsMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

@RestController
public class Suning {
    private String APPKEY = "75a5ef9f00f6560c28f41d7c3831a928";
    private String APPSER = "ff25808f0f2b2873daa72d018e7926d0";
    private String CATECODE = "R9011780";
    private String BRANDCODE = "0M7V";

    @RequestMapping("/")
    public String make() {
        return getSpk();
//        return getProduct();
    }

    @Autowired
    CarsMapper carsMapper;

    @Autowired
    YtCarsMapper ytCarsMapper;

    @RequestMapping("/showStatus")
    public String show(String page,String status){
        itemSaleQueryRequest request = new itemSaleQueryRequest();
        request.setPageNo(Integer.parseInt(page));
        request.setPageSize(50);
//        request.setCategoryCode("R2701002");
//        request.setProductCode("102609882");
        request.setSaleStatus(status);
        //api入参校验逻辑开关，当测试稳定之后建议设置为 false 或者删除该行
        request.setCheckParam(true);
        String serverUrl = "https://open.suning.com/api/http/sopRequest";
        String appKey = APPKEY;
        String appSecret = APPSER;
        DefaultSuningClient client = new DefaultSuningClient(serverUrl, appKey,appSecret, "json");
        try {
            itemSaleQueryResponse response = client.excute(request);
            System.out.println("返回json/xml格式数据 :" + response.getBody());
            return response.getBody();
        } catch (SuningApiException e) {
            return e.getErrMsg();
        }
    }

    @RequestMapping("/editTitle")
    public String edit(){
        List<Cars> cars = carsMapper.selectCarsNotNull();
        for (Cars car : cars) {
            YtCars ytCars = ytCarsMapper.selectYtCarsById(car.getId());
            editTitle(ytCars,car);
        }
        return "执行完毕，本次执行共" + cars.size()+"条记录";
    }

    public void editTitle(YtCars ytCars,Cars cars){
        itemContentsModifyRequest request = new itemContentsModifyRequest();
        request.setProductCode(cars.getSun_id());
        request.setCmTitle("【订金销售】" + ytCars.getCar_model() + " 分期购 二手汽车");
        //api入参校验逻辑开关，当测试稳定之后建议设置为 false 或者删除该行
        request.setCheckParam(true);
        String serverUrl = "https://open.suning.com/api/http/sopRequest";
        String appKey = APPKEY;
        String appSecret = APPSER;
        DefaultSuningClient client = new DefaultSuningClient(serverUrl, appKey,appSecret, "json");
        try {
            itemContentsModifyResponse response = client.excute(request);
            System.out.println("返回json/xml格式数据 :" + response.getBody());
        } catch (SuningApiException e) {
            e.printStackTrace();
        }
    }

    @RequestMapping("/checkSunid")
    public String check(){ //读取数据库并更新sun_id
        List<Cars> cars = carsMapper.selectAllCarsIndex();
        for (Cars car : cars) {
            updateItem(car);
        }
        return "执行完毕，本次更新数据"+ cars.size();
    }

    public void updateItem(Cars cars){
        ItemGetRequest request = new ItemGetRequest();
        request.setItemCode(cars.getCar_id());
        request.setCheckParam(true);
        String serverUrl = "https://open.suning.com/api/http/sopRequest";
        String appKey = APPKEY;
        String appSecret = APPSER;
        DefaultSuningClient client = new DefaultSuningClient(serverUrl, appKey, appSecret, "json");
        try {
            ItemGetResponse response = client.excute(request);
            System.out.println("返回json/xml格式数据 :" + response.getBody());
            String body = response.getBody();
            JSONObject jsonObject = JSONObject.parseObject(body);
            JSONObject responseContent = jsonObject.getJSONObject("sn_responseContent");
            String sn_error = responseContent.getString("sn_error");
            if(sn_error != null){
                return;
            }
            JSONObject sn_body = responseContent.getJSONObject("sn_body");
            JSONObject item = sn_body.getJSONObject("item");
            String productCode = item.getString("productCode");
            String status = item.getString("status");
            System.out.println("productCode:"+productCode+"更新,ID:"+cars.getId());
            carsMapper.updateCarsById(productCode,"更新sun_id,status=" + status, cars.getId());
        } catch (SuningApiException e) {
            e.printStackTrace();
        }
    }

    @RequestMapping("/item")
    public String item(String code) { //根据cars表id查看苏宁状态
        ItemGetRequest request = new ItemGetRequest();
        request.setItemCode(code);
        request.setCheckParam(true);
        String serverUrl = "https://open.suning.com/api/http/sopRequest";
        String appKey = APPKEY;
        String appSecret = APPSER;
        DefaultSuningClient client = new DefaultSuningClient(serverUrl, appKey, appSecret, "json");
        try {
            ItemGetResponse response = client.excute(request);
            System.out.println("返回json/xml格式数据 :" + response.getBody());
            return response.getBody();
        } catch (SuningApiException e) {
            return e.getErrMsg();
        }
    }

    @RequestMapping("/cul")
    public String culDetail() {
        CulitemdetailQueryRequest request = new CulitemdetailQueryRequest();
        request.setItemCode("693806");
        //api入参校验逻辑开关，当测试稳定之后建议设置为 false 或者删除该行
        request.setCheckParam(true);
        String serverUrl = "https://open.suning.com/api/http/sopRequest";
        String appKey = APPKEY;
        String appSecret = APPSER;
        DefaultSuningClient client = new DefaultSuningClient(serverUrl, appKey, appSecret, "json");
        try {
            CulitemdetailQueryResponse response = client.excute(request);
            System.out.println("返回json/xml格式数据 :" + response.getBody());
            return response.getBody();
        } catch (SuningApiException e) {
            return e.getErrMsg();
        }
    }

    @RequestMapping("/product")
    public String product() {
        ProductQueryRequest request = new ProductQueryRequest();
        request.setBrandCode(BRANDCODE);
        request.setCategoryCode(CATECODE);
        request.setPageNo(1);
        request.setPageSize(50);
        //api入参校验逻辑开关，当测试稳定之后建议设置为 false 或者删除该行
        request.setCheckParam(true);
        String serverUrl = "https://open.suning.com/api/http/sopRequest";
        String appKey = APPKEY;
        String appSecret = APPSER;
        DefaultSuningClient client = new DefaultSuningClient(serverUrl, appKey, appSecret, "json");
        try {
            ProductQueryResponse response = client.excute(request);
            System.out.println("返回json/xml格式数据 :" + response.getBody());
            return response.getBody();
        } catch (SuningApiException e) {
            return e.getErrMsg();
        }
    }

    //修改价格
    @RequestMapping("/price")
    public String priceEdit(String code) {
        return editPrice(code);
    }

    public String editPrice(String code) {
        PriceUpdateRequest request = new PriceUpdateRequest();
        request.setDeductiblePrice("500.00");
        request.setPrice("500.00");
        request.setProductCode(code);
        //api入参校验逻辑开关，当测试稳定之后建议设置为 false 或者删除该行
        request.setCheckParam(true);
        String serverUrl = "https://open.suning.com/api/http/sopRequest";
        String appKey = "75a5ef9f00f6560c28f41d7c3831a928";
        String appSecret = "ff25808f0f2b2873daa72d018e7926d0";
        DefaultSuningClient client = new DefaultSuningClient(serverUrl, appKey, appSecret, "json");
        try {
            PriceUpdateResponse response = client.excute(request);
            System.out.println("返回json/xml格式数据 :" + response.getBody() + "。ProductCode:" + code);
            return response.getBody();
        } catch (SuningApiException e) {
            return e.getErrMsg();
        }
    }

    public String getSpk() { //获取我的商品库信息
        ItemQueryRequest request = new ItemQueryRequest();
        request.setCategoryCode("R9011780");
        request.setBrandCode("0M7V");
        request.setStatus("1");
        request.setStartTime("2019-01-01 00:00:00");
        request.setEndTime("2019-08-29 12:00:00");
        request.setPageNo(1);
        request.setPageSize(50);
        request.setCheckParam(true);
        String serverUrl = "https://open.suning.com/api/http/sopRequest";
        String appKey = "";
        String appSecret = "";
        DefaultSuningClient client = new DefaultSuningClient(serverUrl, appKey, appSecret, "json");
        try {
            ItemQueryResponse response = client.excute(request);
            return response.getBody();
        } catch (SuningApiException e) {
            return e.getErrMsg();
        }
    }

    public String getProduct()  //获取苏宁产品信息
    {
        ProductQueryRequest request = new ProductQueryRequest();
        request.setBrandCode("0M7V");
        request.setCategoryCode("R9011780");
        request.setPageNo(1);
        request.setPageSize(50);
        request.setCheckParam(true);
        String serverUrl = "https://open.suning.com/api/http/sopRequest";
        String appKey = "75a5ef9f00f6560c28f41d7c3831a928";
        String appSecret = "ff25808f0f2b2873daa72d018e7926d0";
        DefaultSuningClient client = new DefaultSuningClient(serverUrl, appKey, appSecret, "json");
        try {
            ProductQueryResponse response = client.excute(request);
            System.out.println(response.getBody());
            return response.getBody();
        } catch (Exception e) {
            return e.getMessage();
        }
    }
}
