package com.suning.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.suning.api.DefaultSuningClient;
import com.suning.api.entity.custom.NormModifyRequest;
import com.suning.api.entity.custom.NormModifyResponse;
import com.suning.api.entity.custom.itemSaleQueryRequest;
import com.suning.api.entity.custom.itemSaleQueryResponse;
import com.suning.api.entity.selfmarket.MainproductModifyRequest;
import com.suning.api.entity.selfmarket.MainproductModifyResponse;
import com.suning.api.exception.SuningApiException;
import com.suning.model.YtCarsToIndexCopy1;
import com.suning.service.CarsMapper;
import com.suning.service.YtCarsMapper;
import com.suning.service.YtCarsToIndexCopyMapper;
import com.suning.util.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
public class SunSearchStatusController {
    private String APPKEY = "75a5ef9f00f6560c28f41d7c3831a928";
    private String APPSER = "ff25808f0f2b2873daa72d018e7926d0";
    private String CATECODE = "R9011780";
    private String BRANDCODE = "0M7V";

    @Autowired
    CarsMapper carsMapper;

    @Autowired
    YtCarsMapper ytCarsMapper;

    @Autowired
    YtCarsToIndexCopyMapper ytCarsToIndexCopyMapper;

    @Resource
    private RedisUtil redisUtil;

    @RequestMapping("/upsales")
    public void upSales() {

    }

    @RequestMapping("/modify")
    public void modify() {
//        errCorreContModifyRequest request = new errCorreContModifyRequest();
    }

    @RequestMapping("/searchStatus")
    public void search(String status) {
        for (int i = 190; i <= 372; i++) {
            //商品销售状态。0-停售； 1-在售； 2-待售；3-强制下架；4-违规下架；5-从未上架；6-020协议终止屏蔽中
            itemSaleQueryRequest request = new itemSaleQueryRequest();
            request.setPageNo(i);
            request.setPageSize(50);
//        request.setCategoryCode("R2701002");
//        request.setProductCode("102609882");
            request.setSaleStatus(status);
            //api入参校验逻辑开关，当测试稳定之后建议设置为 false 或者删除该行
            request.setCheckParam(true);
            String serverUrl = "https://open.suning.com/api/http/sopRequest";
            String appKey = APPKEY;
            String appSecret = APPSER;
            DefaultSuningClient client = new DefaultSuningClient(serverUrl, appKey, appSecret, "json");
            try {
                itemSaleQueryResponse response = client.excute(request);
                System.out.println("返回json/xml格式数据 :" + response.getBody());
                String res = response.getBody();
                JSONObject jsonObject = JSONObject.parseObject(res);
                JSONObject responseContent = jsonObject.getJSONObject("sn_responseContent");
                JSONObject sn_body = responseContent.getJSONObject("sn_body");
                JSONArray itemSale = sn_body.getJSONArray("itemSale");
                for (int j = 0; j < itemSale.size(); j++) {
                    JSONObject object = itemSale.getJSONObject(j);
                    YtCarsToIndexCopy1 ytCarsToIndexCopy1 = new YtCarsToIndexCopy1();
                    ytCarsToIndexCopy1.setCar_id((String) object.get("itemCode"));
                    ytCarsToIndexCopy1.setSun_id((String) object.get("productCode"));
                    String saleStatus = (String) object.get("saleStatus");
                    ytCarsToIndexCopy1.setSun_status(Integer.parseInt(saleStatus));
                    addYtCarsToIndexCopy(ytCarsToIndexCopy1);
                }
            } catch (SuningApiException e) {
                e.printStackTrace();
            }
        }
    }

    public void addYtCarsToIndexCopy(YtCarsToIndexCopy1 ytCarsToIndexCopy1) {
        YtCarsToIndexCopy1 cars = ytCarsToIndexCopyMapper.searchByCarId(ytCarsToIndexCopy1.getCar_id());
        if (cars != null) {
            System.out.println(cars.getId());
            return;
        }
        ytCarsToIndexCopyMapper.addIndex(ytCarsToIndexCopy1);
    }

    public void redisHset(String key, String field, Object value) {
        redisUtil.hset(key, field, value);
    }
}
