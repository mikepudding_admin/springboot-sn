package com.suning.controller;

import com.alibaba.fastjson.JSONObject;
import com.suning.model.TaobaoToIndex;
import com.suning.model.YtCars;
import com.suning.service.YtCarsMapper;
import com.suning.service.YtCarsToIndexCopyMapper;
import com.taobao.api.ApiException;
import com.taobao.api.DefaultTaobaoClient;
import com.taobao.api.TaobaoClient;
import com.taobao.api.request.ItemUpdateDelistingRequest;
import com.taobao.api.response.ItemUpdateDelistingResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class TaobaoController {
    public static final String APPKEY = "25851212";
    public static final String SECRETKEY = "22c2be0e0753b57ce12753dab09e4314";
    public static final String SESSION = "61003045cbeb1c386d1926f5937e28fed003e93f5f58b132200735034544";
    public static final String URL = "http://gw.api.taobao.com/router/rest";
    @Autowired
    private YtCarsToIndexCopyMapper ytCarsToIndexCopyMapper;

    @Autowired
    private YtCarsMapper ytCarsMapper;

    @GetMapping("/tbdown")
    public String tbdown(){
        List<TaobaoToIndex> tbs = ytCarsToIndexCopyMapper.searchTb();
        System.out.println("淘宝目前共" + tbs.size() + "条记录");
        int count = 0;
        for (TaobaoToIndex tb : tbs) {
            Integer carid = tb.getCar_id();
            YtCars cars = ytCarsMapper.selectYtCarsById(carid);
            if(cars.getCar_city().equals("南京")){
                System.out.println("本条记录为南京，跳过");
                continue;
            }
            String res = clientDown(tb.getTb_id());
            if(res != null){
                ytCarsToIndexCopyMapper.updateTbStatus(res,tb.getId());
                count++;
            }
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("淘宝目前共" + tbs.size() + "条记录,共删除" + count + "条记录");
        return "淘宝目前共" + tbs.size() + "条记录,共删除" + count + "条记录";
    }

    public String clientDown(Long tbid){
        TaobaoClient client = new DefaultTaobaoClient(URL, APPKEY, SECRETKEY);
        ItemUpdateDelistingRequest req = new ItemUpdateDelistingRequest();
        System.out.println(tbid);
        req.setNumIid(tbid);
        ItemUpdateDelistingResponse rsp = null;
        String isok = null;
        try {
            rsp = client.execute(req, SESSION);
            String rspBody = rsp.getBody();
            JSONObject jsonObject = JSONObject.parseObject(rspBody);
            JSONObject object = jsonObject.getJSONObject("item_update_delisting_response");
            if(object != null){
                System.out.println(object);
                isok = object.toString();
            }
        } catch (ApiException e) {
            e.printStackTrace();
        }
        return isok;
    }
}
