package com.suning.controller;

import com.alibaba.fastjson.JSON;
import com.suning.service.ChesbRepository;
import com.suning.util.CURLUtil;
import com.suning.util.HTTPUtils;
import com.suning.util.MD5Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@RestController
public class YtChesbToCarsController {
    @Autowired
    private ChesbRepository chesbRepository;

    @Value("${chesb.url}")
    private String chesbUrl;
    @Value("${chesb.token}")
    private String token;
    @Value("${chesb.user}")
    private String user;

    @GetMapping("/chesb/show")
    public void  list(){
        Date date = new Date();
        String timestamp = String.valueOf(date.getTime()/1000);
        ChesbSearch chesbSearch = new ChesbSearch();
        chesbSearch.setUser(user);
        chesbSearch.setTime(timestamp);
        String md5 = MD5Util.getMD5(token + timestamp);
        chesbSearch.setCode(md5);
        chesbSearch.setOper("isShow");
        chesbSearch.setId(276475378);
        String jsonString = JSON.toJSONString(chesbSearch);
        System.out.println(jsonString);
        HashMap map = new HashMap();
        map.put("Content-type","application/json");
        String post = null;
        try {
            post = HTTPUtils.post(chesbUrl, map, jsonString);
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(post);
    }
}
